package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

func hasOption(args []string, short string, long string) bool {
	for _, arg := range args {
		if arg == fmt.Sprintf("-%s", short) ||
			arg == fmt.Sprintf("--%s", long) {
			return true
		}
	}
	return false
}

func getOption(args []string, short string, long string) (string, error) {
	for x, arg := range args {
		if arg == fmt.Sprintf("-%s", short) ||
			arg == fmt.Sprintf("--%s", long) {
			if len(args) < x+2 {
				return "", errors.New(fmt.Sprintf("No Value Passed for %s", arg))
			} else {
				return args[x+1], nil
			}
		}
	}
	return "", nil
}

func jsonToString(text []byte, err error) string {
	if err != nil {
		return fmt.Sprintf("\nUnable to Encode to JSON: %s", err)
	} else {
		return string(text)
	}
}

func toJson(obj interface{}, minify bool, indent string) string {
	if minify {
		return jsonToString(json.Marshal(obj))
	} else {
		return jsonToString(json.MarshalIndent(obj, "", indent))
	}
}

func log(line string, writer *bufio.Writer) {
	fmt.Println(line)
	if writer != nil {
		writer.WriteString(line)
	}
}

func main() {
	fi, _ := os.Stdin.Stat()
	if (fi.Mode()&os.ModeCharDevice) == 0 || fi.Size() > 0 {
		// Input is being piped in
		args := os.Args[1:]
		minified := hasOption(args, "m", "minify")
		var err error
		var filename string
		var indent string
		if i, err := getOption(args, "i", "indent"); err != nil || len(i) == 0 {
			indent = "\t"
		} else {
			indent = i
		}
		if filename, err = getOption(args, "f", "file"); err != nil {
			defer fmt.Printf("Could Not Write to file: %s", err)
		}
		var file *bufio.Writer
		if len(filename) > 0 {
			f, _ := os.Create(filename)
			file = bufio.NewWriter(f)
			if file != nil {
				defer file.Flush()
			}
		}
		builder := strings.Builder{}
		reader := bufio.NewReader(os.Stdin)
		for {
			input, _, err := reader.ReadRune()
			if err != nil && err == io.EOF {
				break
			}
			builder.WriteRune(input)
		}
		var result map[string]interface{}
		text := builder.String()
		textBytes := []byte(text)
		if err := json.Unmarshal(textBytes, &result); err != nil {
			// Failed to parse to JSON, try with list object
			var resultList []map[string]interface{}

			if err := json.Unmarshal(textBytes, &resultList); err != nil {
				log(text, file)
				log(fmt.Sprintf("\nUnable to Parse JSON: %s", err), file)
			} else {
				log(toJson(resultList, minified, indent), file)
			}
		} else {
			log(toJson(result, minified, indent), file)
		}
	} else {
		// Input is not coming in through Pipe
		return
	}
}
