# JSON Formatter

Format JSON on the Command line, like the following: 
```
curl https://go.keithm.io/go/news | json
```

## Build

Download this repository and run `go build json.go ; mv ./json /usr/local/bin`

If you have a windows computer, run the go build, then just put it somewhere in your PATH

## Options

### Minify
Pass `-m` or `--minify` to remove formatting from json

### To File
```
curl https://go.keithm.io/go/news | json -f ./file.txt
```
To save output to a file and to stdout

### Different Indentation
```
curl https://go.keithm.io/go/news | json -i "  "
```
To use two spaces to indent rather than a tab 
